#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "SDDrunkDriverEffectWidget.generated.h"

UCLASS()
class SAFEDRIVING_API USDDrunkDriverEffectWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintImplementableEvent)
	void Start();
	UFUNCTION(BlueprintImplementableEvent)
	void Finish();
};
