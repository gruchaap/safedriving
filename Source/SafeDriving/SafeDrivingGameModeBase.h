// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SafeDrivingGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SAFEDRIVING_API ASafeDrivingGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
