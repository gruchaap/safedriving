#include "SafeDriving/Vehicle/DriverEffects/SDDriverEffectBase.h"

void USDDriverEffectBase::Init(ASDVehicleBase* VehicleBase)
{
	Vehicle = VehicleBase;
}

void USDDriverEffectBase::Activate()
{
	bIsActive = true;
}

void USDDriverEffectBase::Deactivate()
{
	bIsActive = false;
}
