#pragma once

#include "CoreMinimal.h"
#include "SDDriverEffectBase.h"
#include "Tickable.h"
#include "SDDrunkDriverEffect.generated.h"

class USDDrunkDriverEffectWidget;

UCLASS()
class SAFEDRIVING_API USDDrunkDriverEffect : public USDDriverEffectBase, public FTickableGameObject
{
	GENERATED_BODY()

public:
	virtual void Activate() override;
	virtual void Deactivate() override;

protected:
	virtual void Tick(float DeltaTime) override;
	virtual bool IsTickable() const override { return bIsTickable; }
	virtual TStatId GetStatId() const override { return TStatId(); }

	UPROPERTY(EditAnywhere)
	TSubclassOf<USDDrunkDriverEffectWidget> DrunkDriverWidgetTemplate;
	UPROPERTY()
	USDDrunkDriverEffectWidget* DrunkDriverWidget;
	UPROPERTY(EditAnywhere)
	float SteeringEffectActiveMinTime = 0.2f;
	UPROPERTY(EditAnywhere)
	float SteeringEffectActiveMaxTime = 0.5f;
	UPROPERTY(EditAnywhere)
	float TimeToSteeringEffectMin = 2.f;
	UPROPERTY(EditAnywhere)
	float TimeToSteeringEffectMax = 5.f;

	bool bIsTickable = false;
	UPROPERTY(EditAnywhere)
	float TimeToSteeringEffectETA = 0.f;
	float SteeringEffectActiveETA = 0.f;
	bool bIsSteeringEffectActive = false;
};
