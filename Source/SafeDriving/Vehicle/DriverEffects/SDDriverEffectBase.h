#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "SDDriverEffectBase.generated.h"

class ASDVehicleBase;

UCLASS(Blueprintable)
class SAFEDRIVING_API USDDriverEffectBase : public UObject
{
	GENERATED_BODY()

public:
	virtual void Init(ASDVehicleBase* VehicleBase);
	virtual void Activate();
	virtual void Deactivate();
	bool IsActive() const { return bIsActive; }

protected:
	UPROPERTY()
	ASDVehicleBase* Vehicle;
	bool bIsActive = false;
};
