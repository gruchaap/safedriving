#include "SafeDriving/Vehicle/DriverEffects/SDDrunkDriverEffect.h"
#include "SafeDriving/UI/DriverEffects/SDDrunkDriverEffectWidget.h"
#include "Blueprint/UserWidget.h"
#include "SafeDriving/Vehicle/SDVehicleBase.h"
#include "Kismet/GameplayStatics.h"

void USDDrunkDriverEffect::Activate()
{
	Super::Activate();

	if (DrunkDriverWidgetTemplate)
	{
		APlayerController* PlayerController = UGameplayStatics::GetPlayerController(Vehicle, 0);
		DrunkDriverWidget = CreateWidget<USDDrunkDriverEffectWidget>(PlayerController, DrunkDriverWidgetTemplate);
		DrunkDriverWidget->AddToViewport();
		DrunkDriverWidget->Start();
	}
	bIsTickable = true;
}

void USDDrunkDriverEffect::Deactivate()
{
	Super::Deactivate();

	if (DrunkDriverWidget)
	{
		DrunkDriverWidget->Finish();
	}
	bIsTickable = false;

	TimeToSteeringEffectETA = 0.f;
	SteeringEffectActiveETA = 0.f;
	bIsSteeringEffectActive = false;
	Vehicle->SetExternalSteeringInput(0.f);
}

void USDDrunkDriverEffect::Tick(float DeltaTime)
{
	if (!bIsSteeringEffectActive)
	{
		if (TimeToSteeringEffectETA <= 0.f)
		{
			bIsSteeringEffectActive = true;
			SteeringEffectActiveETA = FMath::RandRange(SteeringEffectActiveMinTime, SteeringEffectActiveMaxTime);
			float SteeringValue = FMath::RandBool() ? 1.f : -1.f;
			Vehicle->SetExternalSteeringInput(SteeringValue);
		}
		else
		{
			TimeToSteeringEffectETA -= DeltaTime;
		}
	}
	else
	{
		if (SteeringEffectActiveETA > 0.f)
		{
			SteeringEffectActiveETA -= DeltaTime;
		}
		else
		{
			bIsSteeringEffectActive = false;
			TimeToSteeringEffectETA = FMath::RandRange(TimeToSteeringEffectMin, TimeToSteeringEffectMax);
			Vehicle->SetExternalSteeringInput(0.f);
		}
	}
}