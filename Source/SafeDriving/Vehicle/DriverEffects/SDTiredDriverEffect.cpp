#include "SafeDriving/Vehicle/DriverEffects/SDTiredDriverEffect.h"
#include "SafeDriving/Vehicle/SDVehicleBase.h"
#include "Kismet/GameplayStatics.h"
#include "SafeDriving/UI/DriverEffects/SDTiredDriverEffectWidget.h"
#include "TimerManager.h"

void USDTiredDriverEffect::Activate()
{
	Super::Activate();

	if (TiredDriverWidgetTemplate)
	{
		APlayerController* PlayerController = UGameplayStatics::GetPlayerController(Vehicle, 0);
		TiredDriverWidget = CreateWidget<USDTiredDriverEffectWidget>(PlayerController, TiredDriverWidgetTemplate);
		TiredDriverWidget->AddToViewport();
	}
	GetWorld()->GetTimerManager().SetTimer(SleepAtWheelTimerHandle, this, &USDTiredDriverEffect::SleepAtWheel, SleepAfterSeconds);
}

void USDTiredDriverEffect::Deactivate()
{
	Super::Deactivate();

	if (TiredDriverWidget)
	{
		TiredDriverWidget->RemoveFromViewport();
	}
	Vehicle->SetExternalThrottleInput(0.f);
	Vehicle->SetExternalSteeringInput(0.f);

	FTimerManager& TimerManager = GetWorld()->GetTimerManager();
	TimerManager.ClearTimer(SleepAtWheelTimerHandle);
	TimerManager.ClearTimer(WakeUpTimerHandle);
}

void USDTiredDriverEffect::SleepAtWheel()
{
	Vehicle->SetExternalThrottleInput(2.f);
	Vehicle->SetExternalSteeringInput(2.f *(FMath::RandBool() ? 1 : -1));
	GetWorld()->GetTimerManager().SetTimer(WakeUpTimerHandle, this, &USDTiredDriverEffect::WakeUp, WakeUpAfterSeconds);
}

void USDTiredDriverEffect::WakeUp()
{
	Deactivate();
}
