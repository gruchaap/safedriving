#pragma once

#include "CoreMinimal.h"
#include "SafeDriving/Vehicle/DriverEffects/SDDriverEffectBase.h"
#include "SDTiredDriverEffect.generated.h"

class USDTiredDriverEffectWidget;

UCLASS()
class SAFEDRIVING_API USDTiredDriverEffect : public USDDriverEffectBase
{
	GENERATED_BODY()
	
public:
	virtual void Activate() override;
	virtual void Deactivate() override;

protected:
	void SleepAtWheel();
	void WakeUp();

	UPROPERTY(EditAnywhere)
	TSubclassOf<USDTiredDriverEffectWidget> TiredDriverWidgetTemplate;
	UPROPERTY()
	USDTiredDriverEffectWidget* TiredDriverWidget;
	UPROPERTY(EditAnywhere)
	float SleepAfterSeconds = 5.f;
	UPROPERTY(EditAnywhere)
	float WakeUpAfterSeconds = 3.f;

	FTimerHandle SleepAtWheelTimerHandle;
	FTimerHandle WakeUpTimerHandle;
};
