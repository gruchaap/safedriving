#pragma once

#include "CoreMinimal.h"
#include "WheeledVehicle.h"
#include "Engine/EngineTypes.h"
#include "SDVehicleBase.generated.h"

class USpringArmComponent;
class UCameraComponent;
class UPointLightComponent;
class USDDriverEffectsComponent;

UCLASS()
class SAFEDRIVING_API ASDVehicleBase : public AWheeledVehicle
{
	GENERATED_BODY()

public:
	ASDVehicleBase();
	void SetExternalThrottleInput(float Value) { ExternalThrottleInput = Value; }
	void SetExternalSteeringInput(float Value) { ExternalSteeringInput = Value; }

protected:
	virtual void BeginPlay() override;
	void BindInputs();
	UFUNCTION()
	void SetThrottleInput(float Value);
	UFUNCTION()
	void SetSteeringInput(float Value);
	UFUNCTION()
	void SetHandbrakeActive();
	UFUNCTION()
	void SetHandbrakeInactive();
	UFUNCTION(BlueprintCallable)
	USDDriverEffectsComponent* GetDriverEffectsComponent() { return DriverEffectsComponent; }

	void HandleAutomaticLights(float Value);
	void SetBlinkerLeftTimer();
	void SetBlinkerRightTimer();
	void ToggleBlinker(UPointLightComponent* Blinker);

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	USpringArmComponent* BehindCarCameraSpringArm;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UCameraComponent* BehindCarCamera;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UPointLightComponent* BlinkerLeft;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UPointLightComponent* BlinkerRight;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UPointLightComponent* BrakeLightLeft;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UPointLightComponent* BrakeLightRight;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UPointLightComponent* ReverseLightLeft;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UPointLightComponent* ReverseLightRight;
	UPROPERTY(VisibleAnywhere)
	USDDriverEffectsComponent* DriverEffectsComponent;
	UPROPERTY(EditAnywhere, Category = "Car Camera Settings")
	bool bIsDynamicCamera = true;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float BlinkerRate = 0.5f;
	float ExternalThrottleInput = 0.f;
	float ExternalSteeringInput = 0.f;

private:
	FTimerHandle BlinkerLeftTimerHandle;
	FTimerHandle BlinkerRightTimerHandle;
};
