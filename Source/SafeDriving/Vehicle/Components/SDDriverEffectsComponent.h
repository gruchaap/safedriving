#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "SDDriverEffectsComponent.generated.h"

class USDDriverEffectBase;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SAFEDRIVING_API USDDriverEffectsComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	USDDriverEffectsComponent();

protected:
	virtual void BeginPlay() override;
	UFUNCTION(BlueprintCallable)
	void ActivateEffect(TSubclassOf<USDDriverEffectBase> DriverEffectClass);
	UFUNCTION(BlueprintCallable)
	void DeactivateEffect(TSubclassOf<USDDriverEffectBase> DriverEffectClass);
	UPROPERTY(EditAnywhere)
	TArray<TSubclassOf<USDDriverEffectBase>> DriverEffectsTemplates;
	UPROPERTY()
	TArray<USDDriverEffectBase*> DriverEffects;


};
