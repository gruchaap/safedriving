#include "SafeDriving/Vehicle/Components/SDDriverEffectsComponent.h"
#include "SafeDriving/Vehicle/DriverEffects/SDDriverEffectBase.h"
#include "../SDVehicleBase.h"

USDDriverEffectsComponent::USDDriverEffectsComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void USDDriverEffectsComponent::BeginPlay()
{
	Super::BeginPlay();
	ASDVehicleBase* Vehicle = Cast<ASDVehicleBase>(GetOwner());
	if (Vehicle)
	{
		for (TSubclassOf<USDDriverEffectBase> DriverEffectsTemplate : DriverEffectsTemplates)
		{
			USDDriverEffectBase* DriverEffect = NewObject<USDDriverEffectBase>(this, DriverEffectsTemplate);
			DriverEffect->Init(Vehicle);
			DriverEffects.Add(DriverEffect);
		}
	}
}

void USDDriverEffectsComponent::ActivateEffect(TSubclassOf<USDDriverEffectBase> DriverEffectClass)
{
	for (USDDriverEffectBase* DriverEffect : DriverEffects)
	{
		if (DriverEffect->GetClass() == DriverEffectClass)
		{
			if (!DriverEffect->IsActive())
			{
				DriverEffect->Activate();
			}
			return;
		}
	}
}

void USDDriverEffectsComponent::DeactivateEffect(TSubclassOf<USDDriverEffectBase> DriverEffectClass)
{
	for (USDDriverEffectBase* DriverEffect : DriverEffects)
	{
		if (DriverEffect->GetClass() == DriverEffectClass)
		{
			DriverEffect->Deactivate();
			return;
		}
	}
}

