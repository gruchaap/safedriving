#include "SafeDriving/Vehicle/SDVehicleBase.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "Components/PointLightComponent.h"
#include "WheeledVehicleMovementComponent.h"
#include "TimerManager.h"
#include "SafeDriving/Vehicle/Components/SDDriverEffectsComponent.h"

ASDVehicleBase::ASDVehicleBase()
{
	BehindCarCameraSpringArm = CreateDefaultSubobject<USpringArmComponent>("BehindCarCameraSpringArm");
	BehindCarCamera = CreateDefaultSubobject<UCameraComponent>("BehindCarCamera");
	BlinkerLeft = CreateDefaultSubobject<UPointLightComponent>("BlinkerLeft");
	BlinkerRight = CreateDefaultSubobject<UPointLightComponent>("BlinkerRight");
	BrakeLightLeft = CreateDefaultSubobject<UPointLightComponent>("BrakeLightLeft");
	BrakeLightRight = CreateDefaultSubobject<UPointLightComponent>("BrakeLightRight");
	ReverseLightLeft = CreateDefaultSubobject<UPointLightComponent>("ReverseLightLeft");
	ReverseLightRight = CreateDefaultSubobject<UPointLightComponent>("ReverseLightRight");
	DriverEffectsComponent = CreateDefaultSubobject<USDDriverEffectsComponent>("DriverEffectsComponent");
	BehindCarCameraSpringArm->AttachTo(RootComponent);
	BehindCarCamera->AttachTo(BehindCarCameraSpringArm);
	BlinkerLeft->AttachTo(RootComponent);
	BlinkerRight->AttachTo(RootComponent);
	BrakeLightLeft->AttachTo(RootComponent);
	BrakeLightRight->AttachTo(RootComponent);
	ReverseLightLeft->AttachTo(RootComponent);
	ReverseLightRight->AttachTo(RootComponent);
}

void ASDVehicleBase::BeginPlay()
{
	Super::BeginPlay();

	if (bIsDynamicCamera)
	{
		BehindCarCameraSpringArm->bUsePawnControlRotation = false;
		BehindCarCameraSpringArm->bInheritPitch = false;
		BehindCarCameraSpringArm->bInheritYaw = true;
		BehindCarCameraSpringArm->bInheritRoll = false;
		BehindCarCameraSpringArm->bEnableCameraRotationLag = true;
		BehindCarCameraSpringArm->CameraRotationLagSpeed = 5.f;
	}
	BindInputs();
	BlinkerLeft->SetVisibility(false);
	BlinkerRight->SetVisibility(false);
	BrakeLightLeft->SetVisibility(false);
	BrakeLightRight->SetVisibility(false);
	ReverseLightLeft->SetVisibility(false);
	ReverseLightRight->SetVisibility(false);
}

void ASDVehicleBase::BindInputs()
{
	if (InputComponent)
	{
		InputComponent->BindAxis(TEXT("MoveForward"), this, &ASDVehicleBase::SetThrottleInput);
		InputComponent->BindAxis(TEXT("MoveRight"), this, &ASDVehicleBase::SetSteeringInput);

		InputComponent->BindAction(TEXT("Handbrake"), IE_Pressed, this, &ASDVehicleBase::SetHandbrakeActive);
		InputComponent->BindAction(TEXT("Handbrake"), IE_Released, this, &ASDVehicleBase::SetHandbrakeInactive);
		InputComponent->BindAction(TEXT("BlinkerLeft"), IE_Pressed, this, &ASDVehicleBase::SetBlinkerLeftTimer);
		InputComponent->BindAction(TEXT("BlinkerRight"), IE_Pressed, this, &ASDVehicleBase::SetBlinkerRightTimer);
	}
}

void ASDVehicleBase::SetThrottleInput(float Value)
{
	HandleAutomaticLights(Value);
	GetVehicleMovement()->SetThrottleInput(Value + ExternalThrottleInput);
}

void ASDVehicleBase::SetSteeringInput(float Value)
{
	GetVehicleMovement()->SetSteeringInput(Value + ExternalSteeringInput);
}

void ASDVehicleBase::SetHandbrakeActive()
{
	GetVehicleMovement()->SetHandbrakeInput(true);
}

void ASDVehicleBase::SetHandbrakeInactive()
{
	GetVehicleMovement()->SetHandbrakeInput(false);
}

void ASDVehicleBase::HandleAutomaticLights(float Value)
{
	if (Value < 0.f)
	{
		if (GetVehicleMovement()->GetForwardSpeed() >= 0.f)
		{
			BrakeLightLeft->SetVisibility(true);
			BrakeLightRight->SetVisibility(true);
			ReverseLightLeft->SetVisibility(false);
			ReverseLightRight->SetVisibility(false);
		}
		else
		{
			BrakeLightLeft->SetVisibility(false);
			BrakeLightRight->SetVisibility(false);
			ReverseLightLeft->SetVisibility(true);
			ReverseLightRight->SetVisibility(true);
		}
	}
	else
	{
		BrakeLightLeft->SetVisibility(false);
		BrakeLightRight->SetVisibility(false);
		ReverseLightLeft->SetVisibility(false);
		ReverseLightRight->SetVisibility(false);
	}
}

void ASDVehicleBase::SetBlinkerLeftTimer()
{
	if (BlinkerLeftTimerHandle.IsValid())
	{
		GetWorld()->GetTimerManager().ClearTimer(BlinkerLeftTimerHandle);
		BlinkerLeft->SetVisibility(false);
	}
	else
	{
		FTimerDelegate BlinkerDelegate = FTimerDelegate::CreateUObject(this, &ASDVehicleBase::ToggleBlinker, BlinkerLeft);
		GetWorld()->GetTimerManager().SetTimer(BlinkerLeftTimerHandle, BlinkerDelegate, BlinkerRate, true);
		if (BlinkerRightTimerHandle.IsValid())
		{
			GetWorld()->GetTimerManager().ClearTimer(BlinkerRightTimerHandle);
			BlinkerRight->SetVisibility(false);
		}
	}
}

void ASDVehicleBase::SetBlinkerRightTimer()
{
	if (BlinkerRightTimerHandle.IsValid())
	{
		GetWorld()->GetTimerManager().ClearTimer(BlinkerRightTimerHandle);
		BlinkerRight->SetVisibility(false);
	}
	else
	{
		FTimerDelegate BlinkerDelegate = FTimerDelegate::CreateUObject(this, &ASDVehicleBase::ToggleBlinker, BlinkerRight);
		GetWorld()->GetTimerManager().SetTimer(BlinkerRightTimerHandle, BlinkerDelegate, BlinkerRate, true);
		if (BlinkerLeftTimerHandle.IsValid())
		{
			GetWorld()->GetTimerManager().ClearTimer(BlinkerLeftTimerHandle);
			BlinkerLeft->SetVisibility(false);
		}
	}
}

void ASDVehicleBase::ToggleBlinker(UPointLightComponent* Blinker)
{
	if (Blinker)
	{
		Blinker->SetVisibility(!Blinker->IsVisible());
	}
}
