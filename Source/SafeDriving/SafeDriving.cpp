// Copyright Epic Games, Inc. All Rights Reserved.

#include "SafeDriving.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, SafeDriving, "SafeDriving" );
